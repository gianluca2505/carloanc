package business_tier.TO;

public class SedeTO implements InterfaceTO{


	// VARIABILI DI ISTANZA

	private int idSede;
	private String nomeSede;
	private	String citt�Sede;
	private	String indirizzoSede;
	private	String telSede;

	//GETTER AND SETTER
	
	public int getIdSede(){
		return this.idSede;
	}
	
	public void setIdSede(int id){
		this.idSede = id;
	}

	public String getNome() {
		return nomeSede;
	}

	public void setNome(String nome) {
		this.nomeSede = nome;
	}

	public String getCitt�() {
		return citt�Sede;
	}

	public void setCitt�(String citt�) {
		this.citt�Sede = citt�;
	}

	public String getIndirizzo() {
		return indirizzoSede;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzoSede = indirizzo;
	}

	public String getTel() {
		return telSede;
	}

	public void setTel(String tel) {
		this.telSede = tel;
	}

}
