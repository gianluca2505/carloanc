package business_tier.TO;

public class ImpiegatoTO implements InterfaceTO{

	// VARIABILI DI ISTANZA

	private String nomeImp;
	private String cognomeImp;
	private String cfImp;
	private String mailImp;
	private String passwordImp;
	private int sede;


	//GETTER AND SETTER
	
	public String getNome() {
		return nomeImp;
	}
	public void setNome(String nome) {
		this.nomeImp = nome;
	}
	public String getCognome() {
		return cognomeImp;
	}
	public void setCognome(String cognome) {
		this.cognomeImp = cognome;
	}
	public String getCf() {
		return cfImp;
	}
	public void setCf(String cf) {
		this.cfImp = cf;
	}
	public String getMail() {
		return mailImp;
	}
	public void setMail(String mail) {
		this.mailImp = mail;
	}
	public String getPassword() {
		return passwordImp;
	}
	public void setPassword(String password) {
		this.passwordImp = password;
	}
	public int getSede() {
		return sede;
	}
	public void setSede(int sede) {
		this.sede = sede;
	}
	
	
}
