package business_tier.TO;

public class CategoriaTO implements InterfaceTO{
	
	// VARIABILI DI ISTANZA
	
	private int idCategoria;
	private String nomeCategoria;
	private float costoKm;
	private float costoGG;

	//GETTER AND SETTER

	public int getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	public float getCostoKm() {
		return costoKm;
	}
	public void setCostoKm(float costoKm) {
		this.costoKm = costoKm;
	}
	public float getCostoGG() {
		return costoGG;
	}
	public void setCostoGG(float costoGG) {
		this.costoGG = costoGG;
	}
}
