package business_tier.TO;

public class VeicoloTO implements InterfaceTO {


	// VARIABILI DI ISTANZA

	private String targa;
	private String modello;
	private int km;
	private String stato;
	private int sede;
	private int categoria;
	private String alimentazione;
	private int porte;
	private int nPosti;
	private int kmPercorsi;


	//GETTER AND SETTER
	
	/**
	 * Permette di settare i chilometri percorsi.
	 * @param kmPercorsi: i chilometri persi dalla vettura.
	 */
	public void setKmPercrosi(int kmPercorsi){
		this.kmPercorsi = kmPercorsi;
	}
	
	/**
	 * Restituisce i chilometri percorsi.
	 * @return i chilometri percorsi.
	 */
	public int getKmPercorsi(){
		return kmPercorsi;
	}
	
	/**
	 * Restituisce la targa del veicolo.
	 * @return la targa del veicolo.
	 */
	public String getTarga() {
		return targa;
	}
	
	/**
	 * Permette di settare la targa del veicolo.
	 * @param targa: targa del veicolo.
	 */
	public void setTarga(String targa) {
		this.targa = targa;
	}
	
	/**
	 * Restituisce il modello del veicolo.	
	 * @return il modello del veicolo.
	 */
	public String getModello() {
		return modello;
	}
	/**
	 * Permette di settare il modello del veicolo.
	 * @param modello: il modello del veicolo.
	 */
	public void setModello(String modello) {
		this.modello = modello;
	}
	/**
	 * Restituisce i chilometri del veicolo.
	 * @return il numero di chilometri del veicolo.
	 */
	public int getKm() {
		return km;
	}
	
	/**
	 * Permette di settare i chilometri del veicolo.
	 * @param km: i chilometri del veicolo.
	 */
	public void setKm(int km) {
		this.km = km;
	}
	
	/**
	 * Restituisce lo stato del veicolo.
	 * @return lo stato del veicolo.
	 */
	public String getStato() {
		return stato;
	}
	
	/**
	 * Permette di settare lo stato del veicolo.
	 * @param stato: lo stato del veicolo.
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	/**
	 * Restituisce la sede di appartenenza del veicolo.
	 * @return la sede di appartenenza del veicolo.
	 */
	public int getSede() {
		return sede;
	}
	
	/**
	 * Permette di settare la sede di appartenenza del veicolo.
	 * @param sede: la sede di appartenenza del veicolo.
	 */
	public void setSede(int sede) {
		this.sede = sede;
	}
	
	/**
	 * Restituisce la categoria di appartenenza del veicolo.
	 * @return la categoria di appartenenza del veicolo.
	 */
	public int getCategoria() {
		return categoria;
	}
	
	/**
	 * Permette di settare la categoria del veicolo.
	 * @param categoria: la categoria del veicolo.
	 */
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	
	/**
	 * Restituisce il tipo di alimentazione del veicolo.
	 * @return il tipo di alimentazione del veicolo.
	 */
	public String getAlimentazione() {
		return alimentazione;
	}
	
	/**
	 * Permette di settare il tipo di alimentazione del veicolo.
	 * @param alimentazione: il tipo di alimentazione del veicolo.
	 */
	public void setAlimentazione(String alimentazione) {
		this.alimentazione = alimentazione;
	}
	
	/**
	 * Restituisce il numero di porte del veicolo.
	 * @return il numero di porte del veicolo.
	 */
	public int getPorte() {
		return porte;
	}
	
	/**
	 * Permette di settare il numero di porte del veicolo
	 * @param porte: il numero di porte del veicolo.
	 */
	public void setPorte(int porte) {
		this.porte = porte;
	}
	
	/**
	 * Restituisce il numero di posti del veicolo.
	 * @return il numero di posti del veicolo.
	 */
	public int getnPosti() {
		return nPosti;
	}
	
	/**
	 * Permette di settare il numero di posti del veicolo.
	 * @param nPosti: il numero di posti del veicolo.
	 */
	public void setnPosti(int nPosti) {
		this.nPosti = nPosti;
	}
	
	
	
	
}
