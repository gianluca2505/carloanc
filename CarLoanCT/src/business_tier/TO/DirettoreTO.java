package business_tier.TO;

public class DirettoreTO implements InterfaceTO{


	// VARIABILI DI ISTANZA
	
	private String nomeAmmin;
	private String cognomeAmmin;
	private String cfAmmin;;
	private int idSede;
	private String emailAmmin;
	private String passwordAmmin;

	
	//GETTER AND SETTER
	public String getNome() {
		return nomeAmmin;
	}
	public void setNome(String nome) {
		this.nomeAmmin = nome;
	}
	public String getCognome() {
		return cognomeAmmin;
	}
	public void setCognome(String cognome) {
		this.cognomeAmmin = cognome;
	}
	public String getCf() {
		return cfAmmin;
	}
	public void setCf(String cf) {
		this.cfAmmin= cf;
	}
	public int getIdSede() {
		return idSede;
	}
	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}
	public String getEmail() {
		return emailAmmin;
	}
	public void setEmail(String email) {
		this.emailAmmin = email;
	}
	public String getPassword() {
		return passwordAmmin;
	}
	public void setPassword(String password) {
		this.passwordAmmin = password;
	}
	
	
}
