package integration_tier.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import business_tier.Parametri;
import business_tier.TO.CategoriaTO;
import business_tier.TO.ImpiegatoTO;
import business_tier.TO.InterfaceTO;
import integration_tier.Connessione_Database;

public class CategoriaDAO extends Connessione_Database implements InterfacciaDAO  {
	
	@Override
    public void create(Parametri<InterfaceTO> parametro) {
       
        String query = "";
        CategoriaTO cad = (CategoriaTO) parametro.get(0);
       
        int id = cad.getIdCategoria();
        String nome = cad.getNomeCategoria();
        float costoKM = cad.getCostoKm();
        float costoGG = cad.getCostoGG();
       
        query = "INSERT INTO categoria(IDCategoria,nome,costoKm,costo_giornaliero) values('"
                + id + "','" + nome + "','" + costoKM + "','" + costoGG
                + "');";
       
        try{
            connessione();
            stat = conn.createStatement();
            stat.executeUpdate(query);
            JOptionPane.showMessageDialog(null, "Categoria veicolo creata con successo");
            parametro.clear();
        }
        catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Query per la creazione di una categoria fallita.");
            parametro.clear();
        }
    }

	@Override
	public void delete(Parametri<InterfaceTO> parametro) {
		
		CategoriaTO cto = (CategoriaTO) parametro.get(0);
		int categoria = cto.getIdCategoria();	

		ArrayList<Integer> cat = getCategoria();
		if(cat.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti categorie nel DB.");
		}
		else{	

			if(isPresent(categoria, cat) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, la categoria che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM impiegato WHERE IdCategoria='" + categoria + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "La categoria '" + categoria + "' � stata eliminata dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n la categoria : \n '" + categoria + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}
	}

	@Override
	public void update(Parametri<InterfaceTO> paramtro) {
	}

	@Override
	public void search(Parametri<InterfaceTO> parametro) {
		
		CategoriaTO cto = (CategoriaTO) parametro.get(0);
		int categoria = cto.getIdCategoria();	
		
		ArrayList<Integer> cat = getCategoria();
		if(isPresent(categoria, cat) == false){
			JOptionPane.showMessageDialog(null, "Il numero di categoria immesso non � associato ad alcuna auto.");
		}
		else{
			
			String query = "SELECT * FROM carloan.categoria WHERE IdCategoria = '" + categoria + "';";
			try{
				stat = conn.createStatement();
				res = stat.executeQuery(query);
				
				while(res.next()){
					CategoriaTO cto_res = new CategoriaTO();
					cto_res.setCostoGG(res.getFloat("costo_giornaliero")); // OCCHIO PU� ESSERE __ (+ LUNGO NEL DB)
					cto_res.setCostoKm(res.getFloat("costoKm"));
					cto_res.setIdCategoria(res.getInt("IdCategoria"));
					cto_res.setNomeCategoria(res.getString("nome"));
					parametro.add(cto_res);
				}
			}catch(SQLException e ){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query per la ricerca della categori con il seguente id : " + categoria + ", fallita.");
				parametro.clear();
			}
		}
	}

	@Override
	public void list(Parametri<InterfaceTO> parametri) {
		
		ArrayList<Integer> cat = getCategoria();
		
		if(cat.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti categorie nel DB.");
		}
		else{

			String query = "select * from categoria" ;
			try{

				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while (res.next()) {
					CategoriaTO cto_res = new CategoriaTO();
					cto_res.setCostoGG(res.getFloat("costo_giornaliero")); // OCCHIO PU� ESSERE __ (+ LUNGO NEL DB)
					cto_res.setCostoKm(res.getFloat("costoKm"));
					cto_res.setIdCategoria(res.getInt("IdCategoria"));
					cto_res.setNomeCategoria(res.getString("nome"));

				}
				JOptionPane.showMessageDialog(null, "Query per l'ottenimento delle categorie effettuata");

			} catch (SQLException e) {
				parametri = null;
				JOptionPane.showMessageDialog(null, "Query per l'ottenimento delle categorie.");
			}
		}
		
	}

	/**
	 * Genera un array di tipo Integer che contiene le categorie a cui appartengono le auto.
	 * @return un array di tipo Intege contenente le categorie a cui appartengono le auto.
	 */
	private ArrayList<Integer> getCategoria(){

		ArrayList<Integer> cat = new ArrayList<Integer>();

		String query = "select IdCategoria from carloan.categoria;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cat.add(res.getInt("IdCategoria"));
				
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cat;
	}



	/**
	 * Controlla se nell'arrayList � presente la stringa cf passata come parametro formale.
	 * @param cf 
	 * @param cfs
	 * @return true se la stringa cf � contenuta nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(int idSede, ArrayList<Integer> cfs){

		int i = 0;
		boolean present = false;

		while(i < cfs.size() && !present){

			if((cfs.get(i) == idSede)){
				present = true;
		
			}
			i++;
		}
		return present;

	}
	
}
