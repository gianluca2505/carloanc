package integration_tier.DAO;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import business_tier.Parametri;
import business_tier.TO.ClienteTO;
import business_tier.TO.DirettoreTO;
import business_tier.TO.ImpiegatoTO;
import business_tier.TO.InterfaceTO;
import integration_tier.Connessione_Database;

public class ClienteDAO extends Connessione_Database implements InterfacciaDAO  {

	@Override

    public void create(Parametri<InterfaceTO> parametro) {
       
        String query = "";
        ClienteTO cld = (ClienteTO) parametro.get(0);
       
        String cf = cld.getCfCliente();
        String nome = cld.getNome();
        String cognome = cld.getCognome();
        Date dataNascita = cld.getDataNascita();
        int tel = cld.getTellCliente();
        String email = cld.getEmailCliente();
       
        query = "INSERT INTO cliente(cf,Cognome,Nome,DataNascitaCli,TelefonoCli,EmailCli) values('"
                + cf
                + "','"
                + cognome
                + "','"
                + nome
                + "','"
                + dataNascita + "','" + tel + "','" + email + "');";
       
        try{
            getInstance();
            stat = conn.createStatement();
            stat.executeUpdate(query);
            JOptionPane.showMessageDialog(null, "Cliente creato con successo");
            parametro.clear();
        }
       
        catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Query per la creazione cliente fallita");
            parametro.clear();
        }
       
       
    }
	@Override
	public void delete(Parametri<InterfaceTO> parametro) {
		ClienteTO  cto = (ClienteTO) parametro.get(0);
		String cf = cto.getCfCliente();

		ArrayList<String> cfs = getCfs();

		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti clienti nel DB.");
		}
		else{  

			if(isPresent(cf, cfs) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, il cliente che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM cliente WHERE cf='" + cf + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "Il cliente con il CF: '" + cf + "' � stato eliminato dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n Il cliente con il seguente CF: \n '" + cf + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}

	}

	@Override
	public void update(Parametri<InterfaceTO> paramtro) {
		// TODO Auto-generated method stub

	}

	@Override
	public void search(Parametri<InterfaceTO> parametro) {

		ClienteTO cto = (ClienteTO) parametro.get(0);
		String cf = cto.getCfCliente();

		ArrayList<String> cfs = getCfs();
		if(isPresent(cf, cfs) == false){
			JOptionPane.showMessageDialog(null, "Il codice fiscale immesso non � associato ad alcun cliente.");
		}
		else{

			String query = "SELECT * FROM carloan.cliente WHERE cf = '" + cf + "';";
			try{
				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while(res.next()){
					ClienteTO cto_res = new ClienteTO();
					cto_res.setCfCliente(res.getString("cf"));
					cto_res.setCognome(res.getString("Cognome"));
					cto_res.setNome(res.getString("Nome"));
					cto_res.setEmailCliente(res.getString("EmailCli"));
					cto_res.setDataNascita(res.getDate("DataNascitaCli"));
					cto_res.setTellCliente(res.getInt("TelefonoCli"));
					parametro.add(cto_res);
				}
				JOptionPane.showMessageDialog(null, "Query di ricerca eseguita correttamente.");
			}catch(SQLException e ){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query per la ricerca del cliente con il seguente cf : " + cf + ", fallita.");
				parametro.clear();
			}
		}

	}

	@Override
	public void list(Parametri<InterfaceTO> parametro) {

		ArrayList<String> cfs = getCfs();

		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti clienti nel DB.");
		}
		else{

			String query = "select * from cliente";

			try{

				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while (res.next()) {
					ClienteTO cto_res = new ClienteTO();
					cto_res.setCfCliente(res.getString("cf"));
					cto_res.setCognome(res.getString("Cognome"));
					cto_res.setNome(res.getString("Nome"));
					cto_res.setEmailCliente(res.getString("EmailCli"));
					cto_res.setDataNascita(res.getDate("DataNascitaCli"));
					cto_res.setTellCliente(res.getInt("TelefonoCli"));
					parametro.add(cto_res);
				}
				JOptionPane.showMessageDialog(null, "Query per la lista dei clienti effettuata con successo");

			} catch (SQLException e) {
				parametro = null;
				JOptionPane.showMessageDialog(null, "Query per la lista dei clienti errata.");
			}
		}

	}

	/**
	 * Genera un array di tipo String che contiene i cf dei clienti.
	 * @return un array di tipo String contenente i cf dei clienti.
	 */
	private ArrayList<String> getCfs(){

		ArrayList<String> cfs = new ArrayList<String>();

		String query = "select cf from carloan.cliente;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cfs.add(res.getString("cf"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cfs;
	}



	/**
	 * Controlla se nell'arrayList � presente la stringa cf passata come parametro formale.
	 * @param cf 
	 * @param cfs
	 * @return true se la stringa cf � contenuta nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(String cf, ArrayList<String> cfs){

		int i = 0;
		boolean present = false;

		while(i < cfs.size() && !present){

			if((cfs.get(i).compareToIgnoreCase(cf) == 0)){
				present = true;
			}
			i++;
		}
		return present;

	}

}
