package integration_tier.DAO;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Utility.Sessione;
import business_tier.Parametri;
import business_tier.TO.DirettoreTO;
import business_tier.TO.ImpiegatoTO;
import business_tier.TO.InterfaceTO;
import integration_tier.Connessione_Database;

public class DirettoreDAO extends Connessione_Database implements InterfacciaDAO  {

	@Override
	public void create(Parametri<InterfaceTO> parametro) {
		String query="";
		DirettoreTO dto = (DirettoreTO) parametro.get(0);
		String cf = dto.getCf();
		String nome = dto.getNome();
		String cognome = dto.getCognome();
		String email = dto.getEmail();
		String psw = dto.getPassword();
		int idSede = dto.getIdSede();

		ArrayList<String> cfs = new ArrayList<String>();
		cfs = getCfs();


		if(isPresent(cf, cfs) == true){
			JOptionPane.showMessageDialog(null, "Impossibile inserire il seguente impiegato, � gi� presente un impiegato con il seguente cf: \n " + cf);
		}
		else{
			query = "INSERT INTO direttore(cf,nome,cognome,password,mail,id_sede) values ('"
					+ cf
					+ "','"
					+ nome
					+ "','"
					+ cognome
					+ "','"
					+ psw + "','" + email + "'," + idSede + ");";

			try {
				stat = conn.createStatement();
				stat.executeUpdate(query);
				JOptionPane.showMessageDialog(null, "Inserimento avvenuto con successo.");
				parametro.clear();
			} catch (SQLException e) {

				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Inserimento non riuscito, query non riuscita.");
				parametro.clear();
			}
		}
	}



	@Override
	public void delete(Parametri<InterfaceTO> parametro) {

		DirettoreTO  dto = (DirettoreTO) parametro.get(0);
		String cf = dto.getCf();

		ArrayList<String> cfs = getCfs();

		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti direttori nel DB.");
		}
		else{  

			if(isPresent(cf, cfs) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, il direttore che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM direttore WHERE cf='" + cf + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "Il direttore con il CF: '" + cf + "' � stato eliminato dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n Il direttore con il seguente CF: \n '" + cf + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}
	}

	@Override
	public void update(Parametri<InterfaceTO> parametro) {
//		String query = "";
//
//		DirettoreTO dto =  (DirettoreTO) parametro.get(0);
//		String cf = dto.getCf();
//		String nome = dto.getNome();
//		String cognome = dto.getCognome();
//		String mail = dto.getEmail();
//
//		query = "UPDATE direttore SET cf='" + cf + "', nome= '" + nome
//				+ "' ,cognome = '" + cognome + "', mail ='" + mail
//				+ "' where cf = '" + Sessione.getCf() + "';";
//		try{
//			connessione();
//			stat = conn.createStatement();
//			stat.executeUpdate(query);
//			JOptionPane.showMessageDialog(null, "Modifica effettuata con successo");
//		}
//		catch(SQLException e){
//			e.printStackTrace();
//			JOptionPane.showMessageDialog(null, "Query fallita.");
//		}

	}

	@Override
	public void search(Parametri<InterfaceTO> parametro) {

		DirettoreTO dto = (DirettoreTO) parametro.get(0);
		String cf = dto.getCf();
		
		ArrayList<String> cfs = getCfs();
		if(isPresent(cf, cfs) == false){
			JOptionPane.showMessageDialog(null, "Il codice fiscale immesso non � associato ad alcun direttore.");
		}
		else{
			
			String query = "SELECT * FROM carloan.direttore WHERE cf = '" + cf + "';";
			try{
				stat = conn.createStatement();
				res = stat.executeQuery(query);
				
				while(res.next()){
					DirettoreTO ito_res = new DirettoreTO();
					ito_res.setCf(res.getString("cf"));
					ito_res.setCognome(res.getString("cognome"));
					ito_res.setNome(res.getString("nome"));
					ito_res.setEmail(res.getString("mail"));
					ito_res.setPassword(res.getString("password"));
					ito_res.setIdSede(res.getInt("id_sede"));
					parametro.add(ito_res);
				}
			}catch(SQLException e ){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query per la ricerca dell' impiegato con il seguente cf : " + cf + ", fallita.");
				parametro.clear();
			}
		}

	}

	@Override
	public void list(Parametri<InterfaceTO> parametro) {


		ArrayList<String> cfs = getCfs();
		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione,non � presente alcun direttore nel DB.");
		}
		else{

			String query = "select * from carloan.direttore";

			try{

				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while (res.next()) {
					DirettoreTO cat = new DirettoreTO();
					cat.setCf(res.getString("cf"));
					cat.setNome(res.getString("nome"));
					cat.setCognome(res.getString("cognome"));
					cat.setPassword(res.getString("password"));
					cat.setIdSede(res.getInt("id_sede"));
					cat.setEmail(res.getString("mail"));
					parametro.add(cat);
				}
				JOptionPane.showMessageDialog(null, "Query effettuata con successo");

			} catch (SQLException e) {
				parametro = null;
				JOptionPane.showMessageDialog(null, "Query Errata.");
			}
		}

	}


	/**
	 * Genera un array di tipo String che contiene i cf degli impiegati.
	 * @return un array di tipo String contenente i cf degli impiegati.
	 */
	private ArrayList<String> getCfs(){

		ArrayList<String> cfs = new ArrayList<String>();

		String query = "select cf from carloan.direttore;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cfs.add(res.getString("cf"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cfs;
	}



	/**
	 * Controlla se nell'arrayList � presente la stringa cf passata come parametro formale.
	 * @param cf
	 * @param cfs
	 * @return true se la stringa cf � contenuta nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(String cf, ArrayList<String> cfs){

		int i = 0;
		boolean present = false;

		while(i < cfs.size() && !present){

			if((cfs.get(i).compareToIgnoreCase(cf) == 0)){
				present = true;
			}
			i++;
		}
		return present;

	}



}