package integration_tier.DAO;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import business_tier.Parametri;
import business_tier.TO.CategoriaTO;
import business_tier.TO.ContrattoTO;
import business_tier.TO.InterfaceTO;
import integration_tier.Connessione_Database;

public class ContrattoDAO extends Connessione_Database implements InterfacciaDAO  {

	@Override
	public void create(Parametri<InterfaceTO> parametro) {

		String query = "";
		ContrattoTO cod = (ContrattoTO) parametro.get(0);

		int id = cod.getIdContratto();
		Date dataInizio = cod.getDataInizio();
		Date dataFine = cod.getDataFine();
		Date dataConsegna = cod.getDataConsegna();
		float penale = cod.getPenale();
		float acconto = cod.getAcconto();
		float saldo = cod.getSaldo();
		float totale = cod.getImportoTot();
		float kmPercorsi = cod.getKmPercorsi();
		int sedeInizio = cod.getSedeInzio();
		int sedeFine = cod.getSedeFine();
		String targa = cod.getTarga();
		String cliente = cod.getCliente();
		String contratto = cod.getTipoContratto();
		int fasciaKm = cod.getFasciaKm();

		ArrayList<String> cfs = new ArrayList<String>();        
		ArrayList<Integer> ids = new ArrayList<Integer>();

		cfs = getCfCliente();
		ids = getId();
		String cf = cod.getCliente();

		if(isPresent(id, ids) == true){
			JOptionPane.showMessageDialog(null, "Impossibile inserire il seguente id,  " + id + " perch� gi� presente");
		}


		else{
			if(isPresentCf(cf, cfs) == true){
				JOptionPane.showMessageDialog(null, "Attenzione, l'utente con codice fiscale " + "'" + cf + "'"  + " non pu� essere registrato poich� ve n'� gi� uno registrato a suo carico");
			}


			else{

				query = "INSERT INTO contratto(dataInizioCont,dataFineCont,acconto,totaleimporto,sedeInizio,targa,cliente,tipo_contratto,fasciaKm) "
						+ " VALUES('"+ dataInizio + "','"+ dataFine + "', " + acconto  + " , " + totale + " , " + sedeInizio + " ,'"
						+ targa + "','" + cliente  + "','"   + contratto  + "',"  + fasciaKm + ");";



				try {
					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "Inserimento contratto avvenuto con successo.");
					parametro.clear();
				} catch (SQLException e) {

					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Inserimento contratto non riuscito, query non riuscita.");
					parametro.clear();
				}
			}

		}	
	}


	@Override
	public void delete(Parametri<InterfaceTO> parametro) {

		ContrattoTO cto = (ContrattoTO) parametro.get(0);
		//	ContrattoTO cf = (ContrattoTO) parametro.get(1);

		int id = cto.getIdContratto();

		ArrayList<Integer> cat = getId();

		if(cat.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti contratti nel DB.");
		}


		else{	

			if(isPresent(id, cat) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, il contratto che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM contratto WHERE IdContratto='" + id + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "Il contratto con id  '" + id + "' � stato eliminato dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n Il contratto con id : \n '" + id + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}

	}

	@Override
	public void update(Parametri<InterfaceTO> paramtro) {
		// TODO Auto-generated method stub

	}

	@Override
	public void search(Parametri<InterfaceTO> parametri) {


	}

	@Override
	public void list(Parametri<InterfaceTO> parametri) {
		// TODO Auto-generated method stub

	}


	/**
	 * Genera un array di tipo Integer che contiene l'id dei vari contratti sottoscritti.
	 * @return un array di tipo Integer contenente l'id dei vari contratti sottoscritti.
	 */
	private ArrayList<Integer> getId(){

		ArrayList<Integer> cto = new ArrayList<Integer>();

		String query = "select IdContratto from carloan.contratto;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cto.add(res.getInt("IdContratto"));

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cto;
	}



	/**
	 * Controlla se nell'arrayList � presente l'id del contratto passato come parametro formale.
	 * @param cf 
	 * @param cfs
	 * @return true se l'intero id � contenuto nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(int idContratto, ArrayList<Integer> ids){

		int i = 0;
		boolean present = false;

		while(i < ids.size() && !present){

			if((ids.get(i) == idContratto)){
				present = true;

			}
			i++;
		}
		return present;

	}

	/**
	 * Controlla che un cliente possa creare un solo contratto per volta tramite il codice fiscale
	 * @return
	 */
	private ArrayList<String> getCfCliente(){

		ArrayList<String> cfs = new ArrayList<String>();

		String query = "SELECT cf FROM carloan.cliente NATURAL JOIN carloan.contratto;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cfs.add(res.getString("cf"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cfs;
	}	

	private boolean isPresentCf(String cf, ArrayList<String> cfs){

		int i = 0;
		boolean present = false;

		while(i < cfs.size() && !present){

			if((cfs.get(i).compareToIgnoreCase(cf) == 0)){
				present = true;
			}
			i++;
		}
		return present;

	}
}
