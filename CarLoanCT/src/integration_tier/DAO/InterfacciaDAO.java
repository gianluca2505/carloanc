package integration_tier.DAO;
import business_tier.Parametri;
import business_tier.TO.InterfaceTO;

/**
 * L'intefaccia DaoInteface contiene tutti i metodi con cui � possibile manipolare i dati del DB carloan.
 * @author VittorioAlessandro
 *
 */

public interface InterfacciaDAO {
	
	public abstract void create(Parametri<InterfaceTO> parametro);
	
	public abstract void delete(Parametri<InterfaceTO> parametro);

	public abstract void update(Parametri<InterfaceTO> paramtro);
	
	public abstract void search(Parametri<InterfaceTO> parametro);
	
	public abstract void list(Parametri<InterfaceTO> parametro);
}
